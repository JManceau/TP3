// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var jwt = require('jwt-simple');
var md5= require('md5');
var app = express();
var db = new sqlite3.Database('db.sqlite');
  

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });





app.set('jwtTokenSecret', 'YOUR_SECRET_STRING')

//permet de générer un nombre aléatoire entre deux bornes
function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next) {
		

	db.get('SELECT rowid, ident, password FROM users WHERE ident=? AND password=?;', [req.body.login, md5(req.body.mdp)], function(err,data) {
		if(data!=undefined){
			var token = jwt.encode({
			  ident: req.body.login+randomInt(0,99999),
			 password: req.body.mdp
			}, app.get('jwtTokenSecret'));

			db.get('SELECT ident FROM sessions WHERE ident=?', [req.body.login], function(err,data){
			if(data){
			 	db.run('UPDATE sessions SET token=? WHERE ident=?',[token,req.body.login]);
			} 
			else{
				db.run('INSERT INTO sessions (ident,token) values (?,?)',[req.body.login,token]);
			}


});				
			var resp={"status":"true"};
			res.cookie('token',token);
			res.json(resp);

		}
		else{
			var resp={"status":"false"};
			res.json(resp);
		}
	     
	});

});


// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
